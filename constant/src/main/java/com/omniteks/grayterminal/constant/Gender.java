package com.omniteks.grayterminal.constant;

/**
 *
 * @author abdulhakam
 */
public enum Gender {
    MALE, FEMALE;
}
