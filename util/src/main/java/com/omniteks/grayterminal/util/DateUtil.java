package com.omniteks.grayterminal.util;

import java.time.LocalDate;
import java.time.Period;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Nov 20, 2022, at 7:31:02 PM
 */
public class DateUtil {

    public static Integer getAge(LocalDate dateOfBirth) {
        return Period.between(dateOfBirth, LocalDate.now()).getYears();
    }
}
